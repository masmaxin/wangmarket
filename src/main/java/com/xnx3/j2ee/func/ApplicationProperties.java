package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.ApplicationPropertiesUtil;

/**
 * 读取 application.properties 属性，可以直接使用 ApplicationProperties.getProperty("key"); 进行调用
 * @author 管雷鸣
 *
 */
public class ApplicationProperties extends ApplicationPropertiesUtil{
}
